#include <string>
#ifndef TYPE_H
#define TYPE_H
class Type
{
public:
	Type();
	bool GetIsTemp();
	void SetIsTemp(bool isTemp);
	virtual bool isPrintable() const = 0;
	virtual const std::string toString() const = 0;
private:
	bool _isTemp;
};
#endif //TYPE_H
