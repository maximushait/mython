#include "type.h"

Type::Type()
{
	_isTemp = false;
}

bool Type::GetIsTemp()
{
	return _isTemp;
}

void Type::SetIsTemp(bool isTemp)
{
	_isTemp = isTemp;
}
