#include "Helper.h"

bool Helper::isInteger(const std::string& s)
{
	int start = (s[0] == '-') ? 1 : 0;
	for (int i = start; i < s.size(); i++)
	{
		if (!isDigit(s[i]))
		{
			return false;
		}
	}

	return true;
}

bool Helper::isBoolean(const std::string& s)
{
	return (s == "True" || s == "False");
}

bool Helper::isString(const std::string& s)
{
	size_t end = s.size() - 1;

	if (s[0] == '\"' && end == s.find('\"', 1))
	{
		return true;
	}
	if (s[0] == '\'' && end == s.find('\'', 1))
	{
		return true;
	}

	return false;

}

bool Helper::isDigit(char c)
{
	return (c >= '0' && c <= '9');
}

void Helper::trim(std::string &str)
{
	rtrim(str);
	ltrim(str);

}

void Helper::rtrim(std::string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

void Helper::ltrim(std::string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

void Helper::removeLeadingZeros(std::string &str)
{
	size_t startpos = str.find_first_not_of("0");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

bool Helper::Checkifhasequal(std::string& str)
{
	int count = 0;
	for (int i = 0; i < str.size(); i++)
	{
		if (str[i] == '=')
		{
			count++;
		}
	}
	if (count == 1)
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			if (str[i] == '=' && str[i - 1] == 32 && str[i + 1] == 32)
			{
				return true;
				break;

			}
		}
		return false;
	}
	else
	{
		return false;
	}
}

bool Helper::IsList(std::string& str)
{
	if (str[0] != '[' || str[str.size() - 1] != ']')
	{
		return false;
	}
	return true;
}

bool Helper::checkIfType(std::string& str)
{
	std::string first = "type";
	const int FIRST = 0;
	std::size_t found1 = -1;
	found1 = str.find(first);
	if ((found1 >= 0 && found1 <= str.length() && str[FIRST] == 't'))
	{
		return true;
	}
	return false;
}

bool Helper::checkIfDel(std::string& str)
{
	if (str[0] == 'd' && str[1] == 'e' && str[2] == 'l')
	{
		return true;
	}
	return false;
}

bool Helper::checkIfLen(std::string& str)
{
	std::string first = "len";
	const int FIRST = 0;
	const int SECOND = 3;
	std::size_t found1 = -1;
	found1 = str.find(first);
	if ((found1 >= 0 && found1 <= str.length() && str[FIRST] == 'l'))
	{
		return true;
	}
	return false;
}

bool Helper::isLowerLetter(char c)
{
	return (c >= 'a' && c <= 'z');
}

bool Helper::isUpperLetter(char c)
{
	return (c >= 'A' && c <= 'Z');
}

bool Helper::isLetter(char c)
{
	return (isLowerLetter(c) || isUpperLetter(c));
}

bool Helper::isUnderscore(char c)
{
	return ('_' == c);
}

