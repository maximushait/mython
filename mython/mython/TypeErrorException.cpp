#include "TypeErrorException.h"

TypeException::TypeException(std::string name): InterperterException(), _name(name)
{
}
const char* TypeException::what() const throw()
{
	std::string err = "TypeError: object of type'" + _name + "' has no len().";
	return err.c_str();
}
