#include "List.h"

List::List(Type** begin, int size): Sequence(begin, size)
{
	SetIsTemp(true);
}

List::~List()
{
}

bool List::isPrintable() const
{
	return true;
}

const std::string List::toString() const
{
	Type** types = (Type**)_sequence;//chenging it to type pointer
	std::string str = "[";//the start of our string
	unsigned int len = _msize(_sequence) / sizeof(Type**);
	for (int i = 0; i < len; i++)
	{
		str += (types[i])->toString();
		str += ", ";
	}
	str = str.substr(0, str.size() - 2);
	str += "]";
	return str;
}