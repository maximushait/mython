#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : Type
{
public:
	Void(void);
	~Void();
	virtual bool isPrintable() const;
	virtual const std::string toString() const;
};


#endif // VOID_H