#define _CRT_SECURE_NO_WARNINGS
#include "parser.h"
#include <iostream>
#include "IndentationException.h"
std::unordered_map<std::string, Type*> Parser::_vars;

Type* Parser::parseString(std::string str) throw()
{
	Helper::trim(str);
	if (str[0] == 32 || str[0] == 9)
	{
		throw(IndentationException());
	}
	if (str.length() > 0)
	{
		Helper::trim(str);//frist of all triming
		Type* t = getVariableValue(str);//checking if is just var called
		if (t != NULL)
		{
			t->SetIsTemp(false);
			return t;
		}
		else
		{
			Type* type = getType(str);//then getting the type returning
			if (type != NULL)
			{
				return type;
			}
			else
			{
				//if not so its somthing diffrent
				if (makeAssignment(str))//checking if it is an assiment
				{
					Void* typeVoid = new Void();
					((Type*)typeVoid)->SetIsTemp(true);
					return (Type*)typeVoid;
				}
				else if (Helper::checkIfType(str))
				{
					typeOf(str);
					Void* typeVoid = new Void();
					((Type*)typeVoid)->SetIsTemp(true);
					return (Type*)typeVoid;
				}
				else if (Helper::checkIfDel(str))
				{
					del(str);
					Void* typeVoid = new Void();
					((Type*)typeVoid)->SetIsTemp(true);
					return (Type*)typeVoid;
				}
				else if (Helper::checkIfLen(str))
				{
					Type* t = lenOfSequence(str);
					if (t == NULL)
					{
						Void* typeVoid = new Void();
						((Type*)typeVoid)->SetIsTemp(true);
						return (Type*)typeVoid;
					}
					return t;
				}
				throw(NameErrorException(str));
				}
			throw(SyntaxException());
		}
		
	}
	return NULL;
}

Type* Parser::getType(std::string str)
{
	Helper::trim(str);
	if (Helper::isInteger(str))
	{
		Helper::removeLeadingZeros(str);
		integer* type = new integer(std::atoi(str.c_str()));
		//type->SetIsTemp(true);
		return type;
	}
	else if (Helper::isBoolean(str))
	{
		bool boolVal = str.compare("True") == 0 ? true : false;
		boolean* type = new boolean(boolVal);
		return type;
	}
	else if(Helper::isString(str))
	{
		char* s = (char*)malloc(sizeof(char) * str.length() + 1);
		strcpy(s, str.c_str());
		String* string = new String(s, (int)str.length());
		free(s);
		return string;

	}
	else if (Helper::IsList(str))
	{
		std::vector<Type*> types;
		
		
		std::string charchar = ",";

		if (str[0] == '[' && str[1] != ']')
		{
			str.erase(str.begin());//deleting the first char '['
			std::size_t found = str.find(charchar);
			while (found >= 0 && found <= str.length() && str.at(found) == ',')
			{
				std::string temp = str.substr(0, found);
				Type* typetemp = getType(temp);
				types.push_back(typetemp);
				str.erase(0, found + 1);
				//check if the is another two alements
				found = str.find(charchar);
			}
			found = str.find("]");//checks for the last one
			std::string temp = str.substr(0, found);
			Type* typetemp = getType(temp);
			types.push_back(typetemp);
			str.erase(0, found + 1);
			//creating vector of types
		}

		Type** x = (Type**)malloc(sizeof(Type*) * types.size());
		int len = types.size();
		int count = 0;
		for (std::vector<Type*>::iterator i = types.begin(); i != types.end(); i++)
		{
			x[count] = (*i);
			count++;
		}
		List* list = new List(x, (int)types.size());
		free(x);
		return list;
	}
	return NULL;
}

bool Parser::isLegalVarName(std::string str)
{

	if (Helper::isDigit(str[0]))
	{
		return false;
	}
	for (int i = 1; i < str.size(); i++)
	{
		if (!Helper::isDigit(str[i]) && !Helper::isLetter(str[i]) && !Helper::isUnderscore(str[i]))
		{
			return false;
		}
	}
	return true;
}

bool Parser::makeAssignment(std::string str) throw()
{
	Helper::trim(str);
	 if (Helper::Checkifhasequal(str))
	 {
		std::size_t pos = str.find("=");      // position of "live" in str
		std::string param = str.substr(0, pos);
		std::string value = str.substr(pos + 1);
		//triming
		Helper::trim(param);
		Helper::trim(value);
		//first checking the param if is legal
		if (!isLegalVarName(param))
		{
			throw(NameErrorException(param));
		}
		if (checkIFInVars(value) && isLegalVarName(value))
		{//checking if is variable, if though-> deep copy
			/*
			when we copying list we don't need deepcopy, we need to copy the pointers
			*/
			Type* temp = getVariableValue(value);//getting the value
			std::string tostring = "";
			tostring = temp->toString();//getting the string of the list/vars
			if (Helper::IsList(tostring))//if it is list the copying just the pointers
			{
				if (temp != nullptr)
				{

					std::unordered_map<std::string, Type*>::const_iterator got = _vars.find(param);

					if (got == _vars.end())
					{
						std::pair<std::string, Type*> assiment(param, temp);
						_vars.insert(assiment);
						return true;
					}
					else
					{
						_vars[param] = temp;
						return true;
					}
				}
			}
			else
			{
				DeepCopy(str);
			}
			return true;
		}
		Type* type = getType(value);
		if (type != nullptr)
		{

			std::unordered_map<std::string, Type*>::const_iterator got = _vars.find(param);

			if (got == _vars.end())
			{
				std::pair<std::string, Type*> assiment(param, type);
				_vars.insert(assiment);
				return true;
			}
			else
			{
				_vars[param] = type;
				return true;
			}
		}
	}
	return false;
}

Type* Parser::getVariableValue(std::string str)
{
	std::unordered_map<std::string, Type*>::const_iterator got = _vars.find(str);
	//return (got == _variables.end()) ? nullptr : got->second();

	if (got == _vars.end())
	{
		return NULL;
	}
	else
	{
		return got->second;
	}
	return NULL;
}

void Parser::clean()
{
	for (std::unordered_map<std::string, Type*>::iterator i = _vars.begin(); i != _vars.end(); i++)
	{
		delete i->second;
	}
	_vars.clear();
}

void Parser::DeepCopy(std::string str)
{
	Helper::trim(str);
	if (Helper::Checkifhasequal(str))
	{
		std::size_t pos = str.find("=");      // position of "live" in str
		std::string dst = str.substr(0, pos);
		std::string src = str.substr(pos + 1);
		Helper::trim(dst);
		Helper::trim(src);
		//if (!isLegalVarName(src) || !checkIFInVars(src))
		//{
		//	throw NameErrorException(src);
		//}
		//if (!isLegalVarName(dst))
		//{
		//	throw NameErrorException(dst);
		//}
		
		std::unordered_map<std::string, Type*>::const_iterator srciter = _vars.find(src);
		std::unordered_map<std::string, Type*>::const_iterator dstiter = _vars.find(dst);
		if (dstiter == _vars.end())
		{
			std::pair<std::string, Type*> newiter(dst, getType(srciter->second->toString()));
			_vars.insert(newiter);
			//return true;
		}
		else
		{
			_vars[dst] = getType(srciter->second->toString());
			//return true;
		}
		//return false;
	}
	//return false;
}

bool Parser::checkIFInVars(std::string str)
{
	std::unordered_map<std::string, Type*>::const_iterator got = _vars.find(str);

	if (got == _vars.end())
	{
		return false;
	}
	else
	{
		return true;
	}
	return false;
}

integer* Parser::lenOfSequence(std::string str)
{
	if (str == "len")
	{
		std::cout << "<built - in function len>" << std::endl;
		return NULL;
	}
	std::string len = "len(";
	//cuuting the string
	str.erase(0, len.length());
	str.erase(str.length() - 1, 1);
	Type* t = getVariableValue(str);
	if (t == NULL)
	{
		t = getType(str);
		if (t == NULL)
		{
			throw(NameErrorException(str));
		}
	}
	str = t->toString();

	if (Helper::IsList(str))
	{
		int count = 0;
		std::string charchar = ", ";
		std::size_t found = str.find(charchar);
		while (found >= 0 && found <= str.length() && str.at(found) == ',')
		{
			count++;
			str.erase(0, found + 1);
			//check if the is another two alements
			found = str.find(charchar);
		}
		count++;
		return (new integer(count));
	}
	else if (Helper::isString(str))
	{
		return (new integer(str.length() - 2));
	}
	else if (Helper::isInteger(str))
	{
		throw(TypeException("int"));
	}
	else if (Helper::isBoolean(str))
	{
		throw(TypeException("bool"));
	}
	return NULL;
}

void Parser::typeOf(std::string str)
{
	if (str == "type")
	{
		std::cout << "<built - in function type>" << std::endl;
	}
	std::string stringtype = "<type '";
	std::string len = "type(";
	//cuuting the string
	str.erase(0, len.length());
	str.erase(str.length() - 1, 1);
	Type* t = NULL;
	if (Helper::checkIfLen(str))
	{
		t = lenOfSequence(str);
	}
	else
	{
		t = getVariableValue(str);
		if (t == NULL)
		{
			t = getType(str);
			if (t == NULL)
			{
				throw(NameErrorException(str));
			}
		}
	}
	std::string value = t->toString();
	if (Helper::IsList(value))
	{
		stringtype += "list'>";
	}
	else if (Helper::isString(value))
	{
		stringtype += "str'>";
	}
	else if (Helper::isBoolean(value))
	{
		stringtype += "bool'>";
	}
	else //integer
	{
		stringtype += "int'>";
	}
	std::cout << stringtype << std::endl;
}

void Parser::del(std::string str)
{
	if (str == "del")
	{
		std::cout << "<built - in function del>" << std::endl;
	}
	std::string len = "del ";
	//cuuting the string
	str.erase(0, len.length());
	Type* t = NULL;
	t = getVariableValue(str);
	if (t == NULL)
	{
		throw(NameErrorException(str));
	}
	//deleting from the map
	_vars.erase(_vars.find(str));
	delete[] t;
}
