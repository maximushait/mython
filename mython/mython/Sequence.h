#pragma once
#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "type.h"
#include <vector>

template <class T>
class Sequence : public Type
{
public:
	Sequence(T* begin, int size);
	~Sequence();
	virtual bool isPrintable() const = 0;
	virtual const std::string toString() const = 0;
protected:
	T* _sequence;
};
#endif // SEQUENCE_H

template<class T>
inline Sequence<T>::Sequence(T* begin, int size) : Type()
{
	int i = 0;
	_sequence = (T*)malloc(sizeof(T) * size);
	for (i = 0; i < size + 1; i++)
	{
		_sequence[i] = begin[i];
	}
}

template<class T>
inline Sequence<T>::~Sequence()
{
	delete _sequence;
}