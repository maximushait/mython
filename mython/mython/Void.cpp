#include "Void.h"

bool Void::isPrintable() const
{
	return false;
}

const std::string Void::toString() const
{
	return "Void";
}

Void::Void(void): Type()
{

}

Void::~Void()
{
}
