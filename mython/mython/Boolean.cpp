#include "Boolean.h"

boolean::boolean(bool boolean):Type()
{
	_bool = boolean;
	SetIsTemp(true);
}

boolean::~boolean()
{
}

bool boolean::isPrintable() const
{
	return true;;
}

const std::string boolean::toString() const
{
	return _bool == true ? "True" : "False";
}
