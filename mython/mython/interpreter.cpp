#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>
#include "IndentationException.h"
#include"SyntaxException.h"
#include "NameErrorException.h"
#include "TypeErrorException.h"
#define WELCOME "Welcome to My Python Interperter version 1.0 by "
#define YOUR_NAME "Maxim Hait"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;
	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		try
		{
			// prasing command
			Type* type = Parser::parseString(input_string);
			if (type->isPrintable())
			{
				std::cout << type->toString() << std::endl;
			}
			if (type->GetIsTemp())
			{
				delete type;
			}
		}
		catch (const IndentationException &err)
		{
			std::cout << err.what() << std::endl;
		}
		catch (const SyntaxException &err)
		{
			std::cout << err.what() << std::endl;
		}
		catch (const NameErrorException &err)
		{
			std::cout << err.what() << std::endl;
		}
		catch (const TypeException& err)
		{
			std::cout << err.what() << std::endl;
		}
		catch (...)
		{
			std::cout << "error ocurred" << std::endl;
		}
		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}
	Parser::clean();
	return 0;
}


