#pragma once
//#ifndef LIST_H
//#define LIST_H


#include "Sequence.h"
#include "type.h"

class List: public Sequence<Type*>
{
public:
	List(Type** begin, int size);
	~List();
	virtual bool isPrintable() const;
	virtual const std::string toString() const;
private:
};
//#endif //LIST_H