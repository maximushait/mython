#include "String.h"
String::String(char* begin, int size): Sequence(begin, size)
{
	SetIsTemp(true);
}

String::~String()
{
}

bool String::isPrintable() const
{
	return true;
}

const std::string String::toString() const
{
	char* str = _sequence;
	std::string string = str;
	return string;
}
