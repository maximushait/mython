#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence<char>
{
public:
	String(char* begin, int size);
	~String();
	virtual bool isPrintable() const;
	virtual const std::string toString() const;
};

#endif // STRING_H
