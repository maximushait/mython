#include "type.h"
#include <string>


class integer : public Type
{
public:
	integer(int number);
	~integer();
	virtual bool isPrintable() const;
	virtual const std::string toString() const;
private:
	int _number;
};