#ifndef PARSER_H
#define PARSER_H
#include "Integer.h"
#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include "Boolean.h"
#include "SyntaxException.h"
#include "String.h"
#include "NameErrorException.h"
#include "Void.h"
#include "List.h"
#include "TypeErrorException.h"
//#pragma comment(linker, "/ENTRY:ChangedEntry /NODEFAULTLIB /SUBSYSTEM:CONSOLE")
class Parser
{
public:
	//Parser();
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string str);
	static void clean();
	static bool makeAssignment(std::string str) throw();
private:
	static bool isLegalVarName(std::string str);
	
	static Type* getVariableValue(std::string str);

	static void DeepCopy(std::string str);
	static bool checkIFInVars(std::string str);
	static integer* lenOfSequence(std::string);
	static void typeOf(std::string str);
	static void del(std::string str);
	static std::unordered_map<std::string, Type*> _vars;
};

#endif //PARSER_H
