#include "type.h"
#include <string>


class boolean: public Type
{
public:
	boolean(bool boolean);
	~boolean();
	virtual bool isPrintable() const;
	virtual const std::string toString() const;
private:
	bool _bool;
};