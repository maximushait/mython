#ifndef TYPEERROR_EXCEPTION_H
#define TYPEERROR_EXCEPTION_H
#include "InterperterException.h"
#include <string>
class TypeException : public InterperterException
{
public:
	TypeException(std::string name);
	virtual const char* what() const throw();
private:
	std::string _name;
};
#endif //TYPEERROR_EXCEPTION_H