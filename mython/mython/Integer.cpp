#include "Integer.h"

integer::integer(int number):Type(), _number(number)
{
	SetIsTemp(true);
}

integer::~integer()
{
}

bool integer::isPrintable() const
{
	return true;
}

const std::string integer::toString() const
{
	return std::to_string(_number);
}
