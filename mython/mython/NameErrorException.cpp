#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name): InterperterException()
{
	_name = name;
}

const char* NameErrorException::what() const throw()
{
	std::string err = "NameError : name '" + _name + "' is not defined";
	return err.c_str();
}
