#ifndef NAMEERROR_EXCEPTION_H
#define NAMEERROR_EXCEPTION_H
#include "InterperterException.h"
#include <string>
class NameErrorException : public InterperterException
{
public:
	NameErrorException(std::string name);
	virtual const char* what() const throw();
private:
	std::string _name;
};
#endif // NAMEERROR_EXCEPTION_H